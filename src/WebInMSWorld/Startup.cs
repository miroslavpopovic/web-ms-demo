﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebInMSWorld.Startup))]
namespace WebInMSWorld
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
